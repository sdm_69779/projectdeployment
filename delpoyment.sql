-- MySQL dump 10.13  Distrib 8.0.30, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: cdacproject
-- ------------------------------------------------------
-- Server version	8.0.30

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `activities`
--

DROP TABLE IF EXISTS `activities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `activities` (
  `activity_id` int NOT NULL AUTO_INCREMENT,
  `isactivityactive` tinyint(1) DEFAULT '0',
  `activity_address` varchar(100) DEFAULT NULL,
  `activity_name` varchar(100) DEFAULT NULL,
  `activity_rate` double DEFAULT NULL,
  `activity_type` varchar(30) DEFAULT NULL,
  `destinationcity` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`activity_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `activities`
--

LOCK TABLES `activities` WRITE;
/*!40000 ALTER TABLE `activities` DISABLE KEYS */;
INSERT INTO `activities` VALUES (1,0,'Pickup At Hotel','Jodhpur City Tour',2000,'cultural experiences','Jodhpur'),(2,0,'Pickup At Hotel','Jodhpur City Sightseeing Tour With Optional Guide',1499,'cultural experiences','Jodhpur'),(3,0,'Pickup At Hotel','City Ride',1500,'cultural experiences','Pune'),(4,0,'Pick up at Hotel','City Tou of Jodhpur',1300,'cultural experiences','Jodhpur');
/*!40000 ALTER TABLE `activities` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `booked_activities`
--

DROP TABLE IF EXISTS `booked_activities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `booked_activities` (
  `booked_activity_id` int NOT NULL AUTO_INCREMENT,
  `booking_rate_of_activity` double DEFAULT NULL,
  `day_of_activity` int DEFAULT NULL,
  `activity_id` int NOT NULL,
  `booking_id` int NOT NULL,
  PRIMARY KEY (`booked_activity_id`),
  KEY `FKaxspixd51jlqia2yslitj3ln6` (`activity_id`),
  KEY `FK44ahoiepdixrleiqgppurjneu` (`booking_id`),
  CONSTRAINT `FK44ahoiepdixrleiqgppurjneu` FOREIGN KEY (`booking_id`) REFERENCES `bookings` (`booking_id`),
  CONSTRAINT `FKaxspixd51jlqia2yslitj3ln6` FOREIGN KEY (`activity_id`) REFERENCES `activities` (`activity_id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `booked_activities`
--

LOCK TABLES `booked_activities` WRITE;
/*!40000 ALTER TABLE `booked_activities` DISABLE KEYS */;
INSERT INTO `booked_activities` VALUES (1,0,1,1,1),(2,0,2,2,1),(3,0,3,1,1),(4,0,1,1,2),(5,0,2,2,2),(6,0,3,1,2),(7,0,1,1,3),(8,0,2,2,3),(9,0,3,1,3),(10,0,1,2,4),(11,0,2,1,4),(12,0,1,3,5),(13,0,2,3,5),(14,0,3,3,5),(15,0,1,1,6),(16,0,2,2,6),(17,0,3,1,6),(18,0,1,1,7),(19,0,2,2,7),(20,0,3,1,7),(21,0,1,3,8),(22,0,2,3,8);
/*!40000 ALTER TABLE `booked_activities` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `booked_packages_details`
--

DROP TABLE IF EXISTS `booked_packages_details`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `booked_packages_details` (
  `booked_pkg_id` int NOT NULL AUTO_INCREMENT,
  `booking_room_rate` double NOT NULL,
  `destination_city` varchar(50) DEFAULT NULL,
  `is_drop` tinyint(1) DEFAULT '0',
  `is_flight` tinyint(1) DEFAULT '0',
  `no_of_nights` int DEFAULT NULL,
  `package_base_rate` double NOT NULL,
  `pkg_name` varchar(150) DEFAULT NULL,
  `pkg_type` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`booked_pkg_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `booked_packages_details`
--

LOCK TABLES `booked_packages_details` WRITE;
/*!40000 ALTER TABLE `booked_packages_details` DISABLE KEYS */;
INSERT INTO `booked_packages_details` VALUES (1,5000,'Jodhpur',1,1,3,11000,'Awesome Jodhpur ','Couple'),(2,5000,'Jodhpur',1,1,3,11000,'Awesome Jodhpur ','Couple'),(3,5000,'Jodhpur',1,1,3,11000,'Awesome Jodhpur ','Couple'),(4,6000,'Jodhpur',1,1,2,10000,'Vibrant Jodhpur','Family'),(5,6000,'Pune',1,1,3,11000,'Tour to Pune','Family'),(6,5000,'Jodhpur',1,1,3,11000,'Awesome Jodhpur ','Couple'),(7,5000,'Jodhpur',1,1,3,11000,'Awesome Jodhpur ','Couple'),(8,5000,'Pune',1,1,2,8500,'Dynamic Pune','Family');
/*!40000 ALTER TABLE `booked_packages_details` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `bookings`
--

DROP TABLE IF EXISTS `bookings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `bookings` (
  `booking_id` int NOT NULL AUTO_INCREMENT,
  `booking_status` int DEFAULT NULL,
  `from_city` varchar(255) DEFAULT NULL,
  `no_of_guests` int DEFAULT NULL,
  `tour_end_date` date DEFAULT NULL,
  `tour_start_date` date DEFAULT NULL,
  `booked_pkg_id` int DEFAULT NULL,
  `departure_flight_id` int DEFAULT NULL,
  `hotel_id` int DEFAULT NULL,
  `payment_id` int DEFAULT NULL,
  `return_flight_id` int DEFAULT NULL,
  `room_id` int DEFAULT NULL,
  `user_id` int DEFAULT NULL,
  PRIMARY KEY (`booking_id`),
  KEY `FKkp6f8jdwhhdy6qvtfi5uer5qk` (`booked_pkg_id`),
  KEY `FKqy854gp60kmmn41a5pcaiw23q` (`departure_flight_id`),
  KEY `FK7y09f5lun38jnooaw2hch0ke9` (`hotel_id`),
  KEY `FKjki6p9c5yckce7owst8vxu17u` (`payment_id`),
  KEY `FKicap01oce3mib5v6yrypc66v6` (`return_flight_id`),
  KEY `FKrgoycol97o21kpjodw1qox4nc` (`room_id`),
  KEY `FKeyog2oic85xg7hsu2je2lx3s6` (`user_id`),
  CONSTRAINT `FK7y09f5lun38jnooaw2hch0ke9` FOREIGN KEY (`hotel_id`) REFERENCES `hotels` (`hotel_id`),
  CONSTRAINT `FKeyog2oic85xg7hsu2je2lx3s6` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`),
  CONSTRAINT `FKicap01oce3mib5v6yrypc66v6` FOREIGN KEY (`return_flight_id`) REFERENCES `flights` (`flight_id`),
  CONSTRAINT `FKjki6p9c5yckce7owst8vxu17u` FOREIGN KEY (`payment_id`) REFERENCES `payments` (`payment_id`),
  CONSTRAINT `FKkp6f8jdwhhdy6qvtfi5uer5qk` FOREIGN KEY (`booked_pkg_id`) REFERENCES `booked_packages_details` (`booked_pkg_id`),
  CONSTRAINT `FKqy854gp60kmmn41a5pcaiw23q` FOREIGN KEY (`departure_flight_id`) REFERENCES `flights` (`flight_id`),
  CONSTRAINT `FKrgoycol97o21kpjodw1qox4nc` FOREIGN KEY (`room_id`) REFERENCES `rooms` (`room_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bookings`
--

LOCK TABLES `bookings` WRITE;
/*!40000 ALTER TABLE `bookings` DISABLE KEYS */;
INSERT INTO `bookings` VALUES (1,0,'Pune',2,'2023-03-19','2023-03-17',1,1,1,1,1,1,2),(2,0,'Pune',2,NULL,NULL,2,1,1,2,1,1,2),(3,0,'Pune',2,'2023-03-25','2023-03-17',3,1,1,3,1,1,2),(4,0,'Pune',2,NULL,NULL,4,1,2,4,1,5,2),(5,0,'Jodhpur',3,'2023-03-19','2023-03-17',5,3,4,5,3,9,2),(6,0,'Pune',1,'2023-03-18','2023-03-16',6,4,1,6,4,1,3),(7,0,'Pune',1,'2023-03-19','2023-03-17',7,4,1,7,4,1,2),(8,0,'Jodhpur',2,'2023-03-20','2023-03-18',8,5,5,8,5,10,2);
/*!40000 ALTER TABLE `bookings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cities`
--

DROP TABLE IF EXISTS `cities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `cities` (
  `city_id` int NOT NULL AUTO_INCREMENT,
  `city_name` varchar(50) DEFAULT NULL,
  `state` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`city_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cities`
--

LOCK TABLES `cities` WRITE;
/*!40000 ALTER TABLE `cities` DISABLE KEYS */;
INSERT INTO `cities` VALUES (1,'Jodhpur','Rajsthan'),(2,'Pune','Maharashtra'),(3,'Goa','Goa'),(4,'Manali','Himachal Pradesh'),(5,'Keral','Keral');
/*!40000 ALTER TABLE `cities` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `flights`
--

DROP TABLE IF EXISTS `flights`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `flights` (
  `flight_id` int NOT NULL AUTO_INCREMENT,
  `arrival_date_time` datetime(6) DEFAULT NULL,
  `available_seats` int DEFAULT NULL,
  `company_name` varchar(50) DEFAULT NULL,
  `depart_date_time` datetime(6) DEFAULT NULL,
  `dest_city` varchar(50) DEFAULT NULL,
  `flight_code` varchar(50) DEFAULT NULL,
  `flight_duration` int DEFAULT NULL,
  `from_city` varchar(50) DEFAULT NULL,
  `max_cabin_bag_wt` double DEFAULT NULL,
  `max_check_in_bag_wt` double DEFAULT NULL,
  `rate_per_seat` double DEFAULT NULL,
  `total_seats` int DEFAULT NULL,
  PRIMARY KEY (`flight_id`),
  UNIQUE KEY `UK_qyp2ovejidxe2a47q9baa934b` (`flight_code`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `flights`
--

LOCK TABLES `flights` WRITE;
/*!40000 ALTER TABLE `flights` DISABLE KEYS */;
INSERT INTO `flights` VALUES (1,'2023-03-16 16:00:00.000000',10,'Inidian Airline','2023-03-16 15:00:00.000000','Jodhpur','IA858',60,'Pune',8,20,6000,50),(2,'2023-03-17 16:00:00.000000',20,'Indigo','2023-03-17 15:00:00.000000','Pune','IN875',60,'Jodhpur',8,20,6000,50),(3,'2023-03-17 14:34:00.000000',10,'Indian Airline','2023-03-17 13:34:00.000000','Pune','IA859',60,'Jodhpur',8,15,3000,50),(4,'2023-03-17 16:05:00.000000',10,'AIr India','2023-03-17 15:05:00.000000','Jodhpur','IA458',60,'Pune',7,15,3000,50),(5,'2023-03-17 09:08:00.000000',10,'Air India','2023-03-17 08:08:00.000000','Pune','AI589',60,'Jodhpur',7,15,3000,50);
/*!40000 ALTER TABLE `flights` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `guests`
--

DROP TABLE IF EXISTS `guests`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `guests` (
  `guest_id` int NOT NULL AUTO_INCREMENT,
  `dob` date DEFAULT NULL,
  `gender` varchar(255) DEFAULT NULL,
  `guest_first_name` varchar(50) DEFAULT NULL,
  `guest_last_name` varchar(50) DEFAULT NULL,
  `booking_id` int NOT NULL,
  `user_id` int NOT NULL,
  PRIMARY KEY (`guest_id`),
  KEY `FKlxf305bqe4ytpvbygtxlltej2` (`booking_id`),
  KEY `FKfek4t9wt70eakloorogaopg97` (`user_id`),
  CONSTRAINT `FKfek4t9wt70eakloorogaopg97` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`),
  CONSTRAINT `FKlxf305bqe4ytpvbygtxlltej2` FOREIGN KEY (`booking_id`) REFERENCES `bookings` (`booking_id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `guests`
--

LOCK TABLES `guests` WRITE;
/*!40000 ALTER TABLE `guests` DISABLE KEYS */;
INSERT INTO `guests` VALUES (1,'2023-11-11','Female','Shriya','Hanje',1,2),(2,'2023-03-08','Male','Sanket','V',1,2),(3,'2012-12-12','Male','test','test',2,2),(4,'2015-12-13','Male','test','test',2,2),(5,'2023-11-11','Male','akshat','bhujbal',3,2),(6,'2023-11-11','Male','himanshu','lakhapati',3,2),(7,'2001-11-11','Male','Sairaj','Chowgule',4,2),(8,'2005-12-15','Female','Ms.SS','Chougule',4,2),(9,'1990-12-11','Female','Shriya','Hanje',5,2),(10,'1995-01-14','Male','Sanket','H',5,2),(11,'1990-05-14','Male','Pradip','k',5,2),(12,'2011-11-11','Male','asdas','asdaa',6,3),(13,'1998-01-11','Male','Ketan','Kalbhor',7,2),(14,'1995-11-10','Male','Sanket','Vishwasrao',8,2),(15,'1997-12-15','Female','Radhika','Vishwasrao',8,2);
/*!40000 ALTER TABLE `guests` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hotels`
--

DROP TABLE IF EXISTS `hotels`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `hotels` (
  `hotel_id` int NOT NULL AUTO_INCREMENT,
  `img_path` varchar(300) DEFAULT NULL,
  `check_out_time` time DEFAULT NULL,
  `checkin_time` time DEFAULT NULL,
  `destination_city` varchar(50) DEFAULT NULL,
  `is_gym` tinyint(1) DEFAULT '0',
  `is_hotel_active` tinyint(1) DEFAULT '0',
  `hotel_address` varchar(300) DEFAULT NULL,
  `hotel_name` varchar(300) DEFAULT NULL,
  `hotel_type` varchar(50) NOT NULL,
  `is_parking` tinyint(1) DEFAULT '0',
  `hotel_pin` int DEFAULT NULL,
  `is_swimming_pool` tinyint(1) DEFAULT '0',
  `is_wifi` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`hotel_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hotels`
--

LOCK TABLES `hotels` WRITE;
/*!40000 ALTER TABLE `hotels` DISABLE KEYS */;
INSERT INTO `hotels` VALUES (1,'images/hotel/1/240de085-e982-4186-965d-edb86c362113.jpg','11:00:00','12:00:00','Jodhpur',1,1,'Loco Shed Road, Near Bhasker Circle, Ratanada, Ratanada, Jodhpur, India','Ratan Vilas','VILLA',1,342001,1,1),(2,'images/hotel/2/69419855-674d-40d3-bd8f-26681db88988.jpg','10:00:00','11:00:00','Jodhpur',1,1,'Opposite New High Court, Near Shatabdi Circle, Jhalamand, Jodhpur, India.','Fairfield by Marriott Jodhpur','HOTEL',1,342013,1,1),(3,'images/hotel/3/6cf83199-f98e-4f55-a2e6-b351fb619ec3.jpg','10:00:00','11:00:00','Pune',1,1,'Navale Bridge, Ambegaon','Deccan Pavilion','HOTEL',1,411046,1,1),(4,'images/hotel/4/88043b08-e200-4678-8e2b-f6b34f1d6261.jpg','10:00:00','11:00:00','Pune',0,1,'Pune','Le Maredian','HOTEL',1,41101,1,1),(5,'images/hotel/5/1a49907b-7fb9-45ae-be3f-b50980f46548.jpg','10:10:00','11:00:00','Pune',1,1,'Pune Sttaion \\','Hotel Taj','HOTEL',0,41101,1,1);
/*!40000 ALTER TABLE `hotels` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `packages`
--

DROP TABLE IF EXISTS `packages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `packages` (
  `package_id` int NOT NULL AUTO_INCREMENT,
  `img_path` varchar(300) DEFAULT NULL,
  `destinationcity` varchar(15) DEFAULT NULL,
  `isdrop` tinyint(1) DEFAULT '0',
  `isflight` tinyint(1) DEFAULT '0',
  `noofnights` int DEFAULT NULL,
  `ispackageactive` tinyint(1) DEFAULT '0',
  `packagebaserate` double DEFAULT NULL,
  `pkgname` varchar(50) DEFAULT NULL,
  `pkgtype` varchar(255) DEFAULT NULL,
  `hotel_id` int NOT NULL,
  `room_id` int NOT NULL,
  PRIMARY KEY (`package_id`),
  KEY `FKtdvo93d233voyr7g07irkg040` (`hotel_id`),
  KEY `FKjvrgtvp7w2o6yxiq08evwpi4a` (`room_id`),
  CONSTRAINT `FKjvrgtvp7w2o6yxiq08evwpi4a` FOREIGN KEY (`room_id`) REFERENCES `rooms` (`room_id`),
  CONSTRAINT `FKtdvo93d233voyr7g07irkg040` FOREIGN KEY (`hotel_id`) REFERENCES `hotels` (`hotel_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `packages`
--

LOCK TABLES `packages` WRITE;
/*!40000 ALTER TABLE `packages` DISABLE KEYS */;
INSERT INTO `packages` VALUES (1,'images/package/1/a69728ee-dfe2-4860-9c29-dc6b6361fef8.jpg','Jodhpur',1,1,3,1,11000,'Awesome Jodhpur ','Couple',1,1),(2,'images/package/2/b9a30bfe-3606-42a4-8ecd-104db62a208c.jpg','Jodhpur',1,1,2,1,10000,'Vibrant Jodhpur','Family',2,5),(3,'images/package/3/12a9d794-b13c-4d14-836e-595c07be3c00.jpg','Pune',1,1,2,1,7000,'Awesome Pune','Family',3,6),(4,'images/package/4/3bfbf38e-3b32-4202-b994-6d99de20a1c9.jpg','Pune',1,1,3,1,11000,'Tour to Pune','Family',4,9),(5,'images/package/5/2d8204a5-a8ad-44ed-b416-03722703a608.jpg','Pune',1,1,2,1,8500,'Dynamic Pune','Family',5,10);
/*!40000 ALTER TABLE `packages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `payments`
--

DROP TABLE IF EXISTS `payments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `payments` (
  `payment_id` int NOT NULL AUTO_INCREMENT,
  `credit_account_number` varchar(20) DEFAULT NULL,
  `debit_account_number` varchar(20) DEFAULT NULL,
  `mode_of_payment` varchar(50) DEFAULT NULL,
  `payment_date` datetime(6) DEFAULT NULL,
  `payment_status` varchar(255) DEFAULT NULL,
  `total_amount` double DEFAULT NULL,
  `booking_id` int DEFAULT NULL,
  `user_id` int DEFAULT NULL,
  PRIMARY KEY (`payment_id`),
  KEY `FKc52o2b1jkxttngufqp3t7jr3h` (`booking_id`),
  KEY `FKj94hgy9v5fw1munb90tar2eje` (`user_id`),
  CONSTRAINT `FKc52o2b1jkxttngufqp3t7jr3h` FOREIGN KEY (`booking_id`) REFERENCES `bookings` (`booking_id`),
  CONSTRAINT `FKj94hgy9v5fw1munb90tar2eje` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `payments`
--

LOCK TABLES `payments` WRITE;
/*!40000 ALTER TABLE `payments` DISABLE KEYS */;
INSERT INTO `payments` VALUES (1,'57789645888','456686','UPI','2023-03-09 10:02:00.000000','Completed',51000,1,2),(2,'57789645888','12356','DebitCard','2023-03-09 10:14:00.000000','Completed',34000,2,2),(3,'57789645888','56463121','CreditCard','2023-03-09 10:20:00.000000','Completed',34000,3,2),(4,'57789645888','852147','CreditCard','2023-03-09 10:44:00.000000','Completed',32000,4,2),(5,'57789645888','87455896','DebitCard','2023-03-09 14:11:00.000000','Completed',42000,5,2),(6,'57789645888','544545455','DebitCard','2023-03-10 11:37:00.000000','Completed',14000,6,3),(7,'57789645888','12355454','CreditCard','2023-03-10 14:44:00.000000','Completed',14000,7,2),(8,'57789645888','78524698','DebitCard','2023-03-13 07:12:00.000000','Completed',23000,8,2);
/*!40000 ALTER TABLE `payments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pkg_activity`
--

DROP TABLE IF EXISTS `pkg_activity`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `pkg_activity` (
  `pkg_activity_id` int NOT NULL AUTO_INCREMENT,
  `day_of_activity` int DEFAULT NULL,
  `is_active` tinyint(1) DEFAULT '1',
  `activity_id` int DEFAULT NULL,
  `package_id` int DEFAULT NULL,
  PRIMARY KEY (`pkg_activity_id`),
  KEY `FK5owhbov6vjlfl5c1sah4952y1` (`activity_id`),
  KEY `FK2h3vtn0n50yjybxlrs72t2jg8` (`package_id`),
  CONSTRAINT `FK2h3vtn0n50yjybxlrs72t2jg8` FOREIGN KEY (`package_id`) REFERENCES `packages` (`package_id`),
  CONSTRAINT `FK5owhbov6vjlfl5c1sah4952y1` FOREIGN KEY (`activity_id`) REFERENCES `activities` (`activity_id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pkg_activity`
--

LOCK TABLES `pkg_activity` WRITE;
/*!40000 ALTER TABLE `pkg_activity` DISABLE KEYS */;
INSERT INTO `pkg_activity` VALUES (1,1,1,1,1),(2,2,1,2,1),(3,3,1,1,1),(4,1,1,2,2),(5,2,1,1,2),(6,1,1,3,3),(7,2,1,3,3),(8,1,1,3,4),(9,2,1,3,4),(10,3,1,3,4),(11,1,1,3,5),(12,2,1,3,5);
/*!40000 ALTER TABLE `pkg_activity` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rooms`
--

DROP TABLE IF EXISTS `rooms`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `rooms` (
  `room_id` int NOT NULL AUTO_INCREMENT,
  `img_path` varchar(300) DEFAULT NULL,
  `is_ac` tinyint(1) DEFAULT '0',
  `available_rooms` int DEFAULT NULL,
  `is_breakfast_included` tinyint(1) DEFAULT '0',
  `is_room_active` tinyint(1) DEFAULT '0',
  `room_capacity` int DEFAULT NULL,
  `room_rate` double DEFAULT NULL,
  `room_type` varchar(50) NOT NULL,
  `hotel_id` int DEFAULT NULL,
  PRIMARY KEY (`room_id`),
  KEY `FKp5lufxy0ghq53ugm93hdc941k` (`hotel_id`),
  CONSTRAINT `FKp5lufxy0ghq53ugm93hdc941k` FOREIGN KEY (`hotel_id`) REFERENCES `hotels` (`hotel_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rooms`
--

LOCK TABLES `rooms` WRITE;
/*!40000 ALTER TABLE `rooms` DISABLE KEYS */;
INSERT INTO `rooms` VALUES (1,NULL,0,5,1,1,3,5000,'POOL_VILLA',1),(2,NULL,1,5,1,1,2,3000,'STANDARD',1),(3,NULL,1,3,1,1,4,6000,'STUDIO',1),(4,NULL,1,5,1,1,2,5000,'STANDARD',2),(5,NULL,1,2,1,1,4,6000,'STUDIO',2),(6,NULL,1,5,1,1,2,3000,'STANDARD',3),(7,NULL,1,2,1,1,4,5000,'STUDIO',3),(8,NULL,1,10,1,1,3,5000,'STANDARD',4),(9,NULL,1,3,1,1,3,6000,'DELUX',4),(10,NULL,1,10,1,1,2,5000,'STANDARD',5);
/*!40000 ALTER TABLE `rooms` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `users` (
  `user_id` int NOT NULL AUTO_INCREMENT,
  `is_active` tinyint(1) DEFAULT '0',
  `dob` date DEFAULT NULL,
  `email_id` varchar(25) DEFAULT NULL,
  `gender` varchar(255) DEFAULT NULL,
  `mob_no` bigint DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `role` varchar(255) NOT NULL,
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `UK_pwrpg821nujmmnavoq7s420jn` (`email_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,1,'1990-09-16','pradipkhutwad07@gmail.com','male',9850303441,'Pradip Khutwwad','$2a$10$v2eifalvEMhk/U2doQcQcOVYKuDy8ISxV.wl75mhO2zZ6xuwABGIe','ROLE_ADMIN'),(2,1,'1990-11-11','sanket145@gmail.com','Male',9858697485,'Sanket Vishwasrao','$2a$10$ZAln1xhIeXFNPlSECekxj.f.EfxQ7j97wIM3rntYA21.3/KudP55q','ROLE_CUSTOMER'),(3,1,'1990-12-11','sai@gmail.com','Male',8574963258,'Sairaj C','$2a$10$m4cQ8Nl7M85Js25/38.Mhe9P73sjuUpdEkt9pxhoAh8DRMKnVNwWm','ROLE_CUSTOMER');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2023-03-14 13:09:23
